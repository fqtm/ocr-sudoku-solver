#include "../OCR/XOR_NeuralNetwork.h"
#include <stdlib.h>

int main()
{
    //TrainingNeuralNetwork("Data/train_final_light.csv");
    //TestNeuralNetwork("Data/test_final_light.csv");

    init_sdl();
    SDL_Surface* img;
    struct NeuralNetwork network = InitializeNetwork();
    img = load_image("img/test_7.png");
    int result = RecognizeDigit(img,network);
    SDL_Quit();
    printf("Result: %d\n",result);
    return 0;
}

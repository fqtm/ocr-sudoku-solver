#include <stdio.h>
#include <stdlib.h>

int main(){
    //malloc tests
    int* p1 = malloc(sizeof(int));
    *p1 = 666;
    printf("p1 = %p\n", p1);
    printf("*p1 = %d\n", *p1);
    printf("---------------------\n");
    int* p2 = p1;
    printf("p2 = %p\n", p2);
    printf("*p2 = %d\n", *p2);
    printf("=====================\n");
    p1 = malloc(sizeof(int));
    printf("p1 = %p\n", p1);
    printf("*p1 = %d\n", *p1);
    printf("---------------------\n");
    printf("p2 = %p\n", p2);
    printf("*p2 = %d\n", *p2);


    return 0;
}
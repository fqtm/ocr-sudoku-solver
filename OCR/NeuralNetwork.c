#include "NeuralNetwork.h"

struct NeuralNetwork InitializeNetwork()
{
    struct NeuralNetwork network;

    network.nbI = 784;
    network.nbH = 100;
    network.nbO = 9;
    network.LR = 0.01f; // Best 0.01f with 97
    network.Temp = 5050.0f;

    //Initializing all matrices : structure of network
    network.HV =
            CreateMatrix(network.nbH, 1);
    network.OV =
            CreateMatrix(network.nbO, 1);
    //Weights Input-Hidden
    network.WeightsIH =
            CreateMatrix(network.nbH, network.nbI);
    InitRandomMatrix(network.WeightsIH);
    //Weights Hidden-Output
    network.WeightsHO =
            CreateMatrix(network.nbO, network.nbH);
    InitRandomMatrix(network.WeightsHO);
    //Bias Hidden
    network.BH =
            CreateMatrix(network.nbH, 1);
    InitRandomMatrix(network.BH);
    //Bias Output
    network.BO =
            CreateMatrix(network.nbO, 1);
    InitRandomMatrix(network.BO);

    //Forward
    network.SumHO =
            CreateMatrix(network.nbO, 1);

    //Backpropagation
    network.SumIH =
            CreateMatrix(network.nbH,1);

    //Debug1
    network.DOutput  =
            CreateMatrix(network.nbO, 1);
    network.DWHO =
            CreateMatrix(network.nbO, network.nbH);
    network.DHidden =
            CreateMatrix(network.nbH, 1);
    network.DWIH =
            CreateMatrix(network.nbH, network.nbI);

    network.pass =
            CreateMatrix(1,1);
    SetMatrix(network.pass,0,0,0);

    return network;
}


void ForwardPass(struct NeuralNetwork network, struct LabeledData ld, int exam)

{
    // Find the activation's value of hidden neurons
    for (int hidden = 0; hidden < network.nbH; hidden++){
        double SumIH = GetMatrix(network.BH, hidden, 0);
        for (int input = 0; input < network.nbI; input++){
            SumIH +=
                 GetMatrix(network.WeightsIH, hidden, input) *
                 GetMatrix(ld.matpix, input , 0);
        }
        SetMatrix(network.SumIH, hidden, 0, SumIH);
        SetMatrix(network.HV, hidden, 0, Relu(SumIH));
    }

    // Find the activation's value of output neurons
    for (int output = 0; output < network.nbO ; ++output){
        double SumHO = GetMatrix(network.BO, output, 0);
        for (int hidden = 0; hidden < network.nbH; hidden++){
            SumHO += GetMatrix(network.WeightsHO, output, hidden) *
                     GetMatrix(network.HV, hidden, 0);
        }
        SetMatrix(network.SumHO, output, 0, SumHO);
    }

    for (int output = 0; output < network.nbO; output++){
        SetMatrix(network.OV, output, 0,
                  Softmax(network.SumHO, output, network.nbO, network.Temp));
    }

    if(exam == 1){
        if( ld.label == ((double)Max(network.OV))){
            SetMatrix(network.pass,0,0,
                      GetMatrix(network.pass,0,0)+1);
        }
    }

    printf("Label : %f | Output: %d\n",
           ld.label,
           Max(network.OV));
}

int ForwardPassRecognize(struct NeuralNetwork network, struct Matrix matpix)

{
    // Find the activation's value of hidden neurons
    for (int hidden = 0; hidden < network.nbH; hidden++){
        double SumIH = GetMatrix(network.BH, hidden, 0);
        for (int input = 0; input < network.nbI; input++){
            SumIH +=
                    GetMatrix(network.WeightsIH, hidden, input) *
                    GetMatrix(matpix, input , 0);
        }
        SetMatrix(network.SumIH, hidden, 0, SumIH);
        SetMatrix(network.HV, hidden, 0, Relu(SumIH));
    }

    // Find the activation's value of output neurons
    for (int output = 0; output < network.nbO ; ++output){
        double SumHO = GetMatrix(network.BO, output, 0);
        for (int hidden = 0; hidden < network.nbH; hidden++){
            SumHO += GetMatrix(network.WeightsHO, output, hidden) *
                     GetMatrix(network.HV, hidden, 0);
        }
        SetMatrix(network.SumHO, output, 0, SumHO);
    }

    for (int output = 0; output < network.nbO; output++){
        SetMatrix(network.OV, output, 0,
                  Softmax(network.SumHO, output, network.nbO, network.Temp));
    }

    return  Max(network.OV);
}

void BackPropagation(struct NeuralNetwork network, struct LabeledData ld)
{
    //Part 1
    //Find Gradient for each weights HO and Bias O
    for (int output = 0; output < network.nbO; ++output){
        double DO = GetMatrix(network.OV, output, 0) - ld.onehot[output];
        SetMatrix(network.DOutput, output, 0, DO);
        for (int hidden = 0; hidden < network.nbH; ++hidden){
            SetMatrix(network.DWHO, output, hidden,
                      DO * GetMatrix(network.HV,hidden,0));
        }
    }

    //Find Gradient for each weigths IH and Bias H
    for (int hidden = 0; hidden < network.nbH ; ++hidden){
        double DH = 0;
        for (int output = 0; output < network.nbO ; ++output){
            DH += GetMatrix(network.WeightsHO, output, hidden)
                    * GetMatrix(network.DOutput, output, 0);
        }
        SetMatrix(network.DHidden, hidden, 0,
                  DH * dRelu(GetMatrix(network.SumIH,hidden,0)));
    }
    for (int hidden = 0; hidden < network.nbH ; ++hidden){
        for (int input = 0; input < network.nbI; ++input){
            SetMatrix(network.DWIH, hidden, input,
                      GetMatrix(network.DHidden,hidden,0)
                                 * GetMatrix(ld.matpix, input, 0));
        }
    }

    // Part 2
    // Update WeightHO and Bias O
    for (int output = 0; output < network.nbO ; ++output) {
        for (int hidden = 0; hidden < network.nbH; ++hidden) {
            SetMatrix(network.WeightsHO, output, hidden,
                 GetMatrix(network.WeightsHO, output, hidden)
                    - network.LR * GetMatrix(network.DWHO, output, hidden));
        }
        SetMatrix(network.BO,output,0,
                  GetMatrix(network.BO,output,0)
                  - network.LR * GetMatrix(network.DOutput,output,0));
    }

    // Update WeightIH and Bias H
    for (int hidden = 0; hidden < network.nbH ; ++hidden) {
        for (int input = 0; input < network.nbI; ++input) {
            SetMatrix(network.WeightsIH, hidden, input,
                  GetMatrix(network.WeightsIH, hidden, input)
                  - network.LR * GetMatrix(network.DWIH, hidden, input));
        }
        SetMatrix(network.BH, hidden,0,
                  GetMatrix(network.BH,hidden,0)
                  - network.LR * GetMatrix(network.DHidden,hidden,0));
    }
}

void SaveNetWorkWIH(struct NeuralNetwork network,const char* dest) {
    FILE *dst = fopen(dest, "w");

    //Save Mat WIH
    for (int hidden = 0; hidden < network.nbH - 1; ++hidden) {
        for (int input = 0; input < network.nbI; ++input) {

            double num = GetMatrix(network.WeightsIH, hidden, input);
            char outputs[50];

            snprintf(outputs, 50, "%f", num);

            fputs(outputs, dst);
            fputc(',', dst);
        }
    }
    for (int input = 0; input < network.nbI - 1; ++input) {

        double num = GetMatrix(network.WeightsIH, network.nbH - 1, input);
        char outputs[50];
        snprintf(outputs, 50, "%f", num);

        fputs(outputs, dst);
        putc(',', dst);
    }

    //Last with \n
    double num = GetMatrix(network.WeightsIH, network.nbH - 1, network.nbI - 1);
    char outputs[50];
    snprintf(outputs, 50, "%f", num);
    fputs(outputs, dst);
    fclose(dst);
}

void SaveNetWorkWHO(struct NeuralNetwork network,const char* dest) {
    FILE *dst = fopen(dest, "w");

    //Save Mat WHO
    for (int output = 0; output < network.nbO-1; ++output) {
        for (int hidden = 0; hidden < network.nbH; ++hidden) {

            double num = GetMatrix(network.WeightsHO,output,hidden);
            char outputs[50];

            snprintf(outputs, 50, "%f", num);

            fputs(outputs,dst);
            fputc(',', dst);
        }
    }
    for (int hidden = 0; hidden < network.nbH-1; ++hidden) {

        double num = GetMatrix(network.WeightsHO,network.nbO-1,hidden);
        char outputs[50];
        snprintf(outputs, 50, "%f", num);

        fputs(outputs,dst);
        putc(',', dst);
    }

    //Last with \n
    double num = GetMatrix(network.WeightsHO,network.nbO-1,network.nbH-1);
    char outputs[50];
    snprintf(outputs, 50, "%f", num);
    fputs(outputs,dst);
    fclose(dst);
}

void SaveNetWorkBH(struct NeuralNetwork network,const char* dest) {
    FILE *dst = fopen(dest, "w");

    //Bias H
    for (int hidden = 0; hidden < network.nbH-1 ; ++hidden) {
        double num = GetMatrix(network.BH,hidden,0);
        char outputs[50];
        snprintf(outputs, 50, "%f", num);
        fputs(outputs,dst);
        putc(',', dst);
    }

    double num = GetMatrix(network.BH,network.nbH-1,0);
    char outputs[50];
    snprintf(outputs, 50, "%f", num);
    fputs(outputs,dst);
    fclose(dst);
}

void SaveNetWorkBO(struct NeuralNetwork network,const char* dest) {
    FILE *dst = fopen(dest, "w");

    //Bias O
    for (int output = 0; output < network.nbO-1 ; ++output) {
        double num = GetMatrix(network.BO,output,0);
        char outputs[50];
        snprintf(outputs, 50, "%f", num);
        fputs(outputs,dst);
        putc(',', dst);
    }

    double num = GetMatrix(network.BO,network.nbO-1,0);
    char outputs[50];
    snprintf(outputs, 50, "%f", num);
    fputs(outputs,dst);
    fclose(dst);
}


void ImportNetwork(const char* WIH, const char* WHO,
                   const char* BH, const char* BO,struct NeuralNetwork network)
                           {
    FILE* src = fopen(WIH, "r");
    if(src == NULL){
        printf("Impossible to open src in Import");
        return;
    }
    int currentchar = fgetc(src);
    double value = 0;
    int is_negative = 0;
    int is_dec = 0;
    double dotdec = 10.0f;
    size_t i = 0;
    int pos_mat = 0;
    //Set Up MatWIH
    while (currentchar != EOF){
        if(currentchar == '-'){
            is_negative = 1;
        }
        else if(currentchar == '.'){
            is_dec = 1;
            dotdec = 10.0f;
        }
        else if(currentchar == ','){
            if(is_negative == 1){
                value *= -1;
                is_negative = 0;
            }
            SetMatrix(network.WeightsIH,pos_mat/784,pos_mat%784,value);
            pos_mat++;
            value = 0;
            is_dec = 0;
        }
        else{
            if(is_dec == 0){
                value = value*10 + (double)(currentchar-'0');
            }
            else
            {
                value += (double)(currentchar-'0') / dotdec;
                dotdec *=10;
            }
        }
        currentchar = fgetc(src);
        i++;
    }
    //Last value from line 1
    if(is_negative == 1){
        value *= -1;
        is_negative = 0;
    }
    SetMatrix(network.WeightsIH,pos_mat/784,pos_mat%784,value);
    value = 0;
    is_dec = 0;
    pos_mat = 0;
    fclose(src);



    //SetUp Mat WHO
    src = fopen(WHO, "r");
    currentchar = fgetc(src);
    while (currentchar != EOF){
        if(currentchar == '-')
            is_negative = 1;
        else if(currentchar == '.'){
            is_dec = 1;
            dotdec = 10.0f;
        }
        else if(currentchar == ','){
            if(is_negative == 1){
                value *= -1;
                is_negative = 0;
            }
            SetMatrix(network.WeightsHO,pos_mat/100,pos_mat%100,value);
            pos_mat++;
            value = 0;
            is_dec = 0;
        }
        else{
            if(is_dec == 0){
                value = value*10 + (double)(currentchar-'0');
            }
            else
            {
                value += (double)(currentchar-'0') / dotdec;
                dotdec *=10;
            }
        }
        currentchar = fgetc(src);
    }
    if(is_negative == 1){
        value *= -1;
        is_negative = 0;
    }
    SetMatrix(network.WeightsHO,pos_mat/100,pos_mat%100,value);
    value = 0;
    is_dec = 0;
    pos_mat = 0;
    fclose(src);

    //SetUp Mat BH
    src = fopen(BH, "r");
    currentchar = fgetc(src);

    while (currentchar != EOF){
        if(currentchar == '-')
            is_negative = 1;
        else if(currentchar == '.'){
            is_dec = 1;
            dotdec = 10.0f;
        }
        else if(currentchar == ','){
            if(is_negative == 1){
                value *= -1;
                is_negative = 0;
            }
            SetMatrix(network.BH,pos_mat,0,value);
            value = 0;
            is_dec = 0;
            pos_mat++;
        }
        else{
            if(is_dec == 0){
                value = value*10 + (double)(currentchar-'0');
            }
            else
            {
                value += (double)(currentchar-'0') / dotdec;
                dotdec *=10;
            }
        }
        currentchar = fgetc(src);
    }
    //Last value from line 3
    if(is_negative == 1){
        value *= -1;
        is_negative = 0;
    }
    SetMatrix(network.BH,pos_mat,0,value);
    value = 0;
    pos_mat = 0;
    is_dec = 0;
    fclose(src);

    //SetUp Mat BO
    src = fopen(BO, "r");
    if(src == NULL){
        printf("Impossible to open BO");
        return;
    }
    currentchar = fgetc(src);

    while (currentchar != EOF){
        if(currentchar == '-')
            is_negative = 1;
        else if(currentchar == '.'){
            is_dec = 1;
            dotdec = 10.0f;
        }
        else if(currentchar == ','){
            if(is_negative == 1){
                value *= -1;
                is_negative = 0;
            }
            SetMatrix(network.BO,pos_mat,0,value);
            value = 0;
            is_dec = 0;
            pos_mat++;
        }
        else{
            if(is_dec == 0){
                value = value*10 + (double)(currentchar-'0');
            }
            else
            {
                value += (double)(currentchar-'0') / dotdec;
                dotdec *=10;
            }
        }
        currentchar = fgetc(src);
    }
    //Last value from line 3
    if(is_negative == 1){
        value *= -1;
        is_negative = 0;
    }
    SetMatrix(network.BO,pos_mat,0,value);
    fclose(src);
}

// Function that train the neural network
void TrainingNeuralNetwork(const char* train)
{
    struct NeuralNetwork network = InitializeNetwork();

    // Start the training of AI
    for(int epoch = 0; epoch < 4; epoch++)
    {
        FILE* src = fopen(train, "r");
        if(src == NULL){
            printf("Impossible to open src in Train");
            return;
        }
        char line[788];
        struct Matrix pixmat =
                CreateMatrix(784,1);

        while (fgets(line, 788, src) != NULL){
            int label = line[0] - '0';
            SetMatrixFromLine(line, pixmat);
            struct LabeledData ld =
                    CreateLD( label, pixmat);
            ForwardPass(network, ld, 0);
            BackPropagation(network, ld);
        }
        fclose(src);
        epoch++;
    }

    //Save parameters
    SaveNetWorkWIH(network,"wih.txt");
    SaveNetWorkWHO(network,"who.txt");
    SaveNetWorkBH(network,"bh.txt");
    SaveNetWorkBO(network,"bo.txt");
}

void TestNeuralNetwork(const char* exam){
    struct NeuralNetwork network = InitializeNetwork();
    ImportNetwork("wih.txt", "who.txt", "bh.txt", "bo.txt", network);
    FILE* src = fopen(exam, "r");
    if(src == NULL){
        printf("Impossible to open exam in Test");
        return;
    }
    char line[788];
    struct Matrix pixmat =
            CreateMatrix(784,1);
    int nb_test = 1;
    while (fgets(line, 788, src) != NULL){
        int label = line[0] - '0';
        SetMatrixFromLine(line, pixmat);
        struct LabeledData ld =
                CreateLD( label, pixmat);
        ForwardPass(network, ld, 1);
        BackPropagation(network, ld);
        nb_test++;
    }
    int pass = ((int)GetMatrix(network.pass,0,0));
    printf("%d test passés  sur %d test \n", pass, nb_test);
    fclose(src);
}

int RecognizeDigit(SDL_Surface* img, struct NeuralNetwork network){
    struct Matrix matpix = CreateMatrix(784,1);
    SetMatFromImg(matpix,img);
    return ForwardPassRecognize(network, matpix);
}

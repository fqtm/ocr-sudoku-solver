//
// Created by bdaum on 04/10/2021.
//

#ifndef SUDOKU_SOLVER_H
#define SUDOKU_SOLVER_H

#include <stddef.h>
#include <stdio.h>

void Save(size_t grid[][9], char file_name[]);

void Load(char load_file[], size_t grid[][9]);

int SudoSolver(size_t grid[][9],size_t row, size_t col);

#endif //SUDOKU_SOLVER_H

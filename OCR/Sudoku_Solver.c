//
// Created by bdaum on 13/11/2021.
//
#include "Solver.h"
#include "NeuralNetwork.h"
#include "pre_processing.h"
#include "Grid_getter.h"
#include <gtk/gtk.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>


//Gtk element
GtkWidget *Window;
GtkWidget *buttonFile;
GtkWidget *buttonSolve;
GtkWidget *buttonSave;
GtkGrid *Grid;

GtkBuilder *builder;
GdkPixbuf *pixbuf;

//General
char *File;
size_t grid[9][9];
struct NeuralNetwork network;

void Warning(char Title[],char description[])
{
    GtkWidget *dialog;
    dialog = gtk_message_dialog_new(Window,GTK_DIALOG_DESTROY_WITH_PARENT,GTK_MESSAGE_INFO,GTK_BUTTONS_OK,Title);
    gtk_message_dialog_format_secondary_text(dialog,description);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

void Print_grid()
{
    for (int i = 1; i <= 9; ++i) {
        for (int j = 1; j <= 9; ++j) {
            GtkWidget *temp = gtk_grid_get_child_at(Grid,i,j);
            if(grid[i][j] !=0)
            {
                char str[] = "0";
                str[0] = (char) grid[i][j] + '0';
                gtk_label_set_label(GTK_LABEL(temp),str);
            }
            else
                gtk_label_set_label(GTK_LABEL(temp),"X");

        }
    }

}


void open_dialog(GtkWidget* button, gpointer window)
{
    GtkWidget *dialog;
    dialog = gtk_file_chooser_dialog_new("Chosse a file", GTK_WINDOW(window), GTK_FILE_CHOOSER_ACTION_OPEN, GTK_STOCK_OK
                                         , GTK_RESPONSE_OK, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);
    gtk_widget_show_all(dialog);
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), g_get_home_dir());
    gint resp = gtk_dialog_run(GTK_DIALOG(dialog));
    if(resp == GTK_RESPONSE_OK) {

        File = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));

        SDL_Surface* binary;
        SDL_Surface* sobel;

        sobel = filter(File, &binary);

        SDL_Surface* cells = Get_cells(sobel, binary);

        for (size_t i = 0; i < 81; i++){
            if (is_black(&cells[i])){
                grid[i/9][i%9] = 0;
            }
            else{
                grid[i/9][i%9] = (size_t) RecognizeDigit(&cells[i], network);
            }
        }

        Load(File, grid);
        Print_grid();
        gtk_widget_set_sensitive(buttonSolve,TRUE);
        gtk_widget_set_sensitive(buttonSave,TRUE);
    }
    gtk_widget_destroy(dialog);
    Warning("Wrong File", "Only PNG and JPEG are allowed ");
}

void on_Solve(GtkWidget* button, gpointer window)
{

    if(SudoSolver(grid, 0, 0))
    {
        Print_grid();
        Warning("Solving Completed","");
    }
    else printf("\n\nNO SOLUTION\n\n");


}

void on_save(GtkWidget* button, gpointer window){
    GtkWidget *dialog;
    GtkFileChooser *chooser;
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
    gint res;

    dialog = gtk_file_chooser_dialog_new ("Save File",Window,action,("Cancel"),GTK_RESPONSE_CANCEL,("Save"),
                                          GTK_RESPONSE_ACCEPT,NULL);
    chooser = GTK_FILE_CHOOSER (dialog);

    gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);
    gtk_file_chooser_set_current_name (chooser,("Untitled document"));
    res = gtk_dialog_run (GTK_DIALOG (dialog));
    if (res == GTK_RESPONSE_ACCEPT)
    {
        char *filename;
        filename = gtk_file_chooser_get_filename (chooser);
        Save(grid,filename);
        g_free (filename);
    }

    gtk_widget_destroy (dialog);
}


int main(int argc,char* argv[])
{
    gtk_init(&argc, &argv);
    builder = gtk_builder_new ();

    GError* error = NULL;
    if (gtk_builder_add_from_file(builder, "interface.glade", &error) == 0)
    {
        g_printerr("Error loading file: %s\n", error->message);
        g_clear_error(&error);
        return 1;
    }


    //Window
    Window = GTK_WIDGET(gtk_builder_get_object(builder, "Window"));
    g_signal_connect(Window,"destroy",G_CALLBACK(gtk_main_quit),NULL);

    //FileButton
    buttonFile = GTK_WIDGET(gtk_builder_get_object(builder,"ButtonFile"));
    g_signal_connect(buttonFile,"clicked",G_CALLBACK(open_dialog),Window);


    //SolveButton
    buttonSolve = GTK_WIDGET(gtk_builder_get_object(builder,"ButtonSolve"));
    g_signal_connect(buttonSolve,"clicked",G_CALLBACK(on_Solve),Window);
    gtk_widget_set_sensitive(buttonSolve,FALSE);

    //SaveButton
    buttonSave = GTK_WIDGET(gtk_builder_get_object(builder,"ButtonSave"));
    g_signal_connect(buttonSave,"clicked",G_CALLBACK(on_save),Window);
    gtk_widget_set_sensitive(buttonSave,FALSE);

    //Grid
    Grid = GTK_GRID(gtk_builder_get_object(builder,"Grid"));


    gtk_widget_show(Window);

    gtk_main();

    network = InitializeNetwork();
    ImportNetwork("wih.txt", "who.txt", "bh.txt", "bo.txt", network);

    return 1;
}


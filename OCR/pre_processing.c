//
// Created by mmala on 04/10/2021.
//
#include "pre_processing.h"
#include "ImageProc.h"
#include "filter.h"
#include <stdio.h>


#include <err.h>


SDL_Surface *filter(char *path, SDL_Surface** binary){

    SDL_Surface* img;
    SDL_Surface *bin;



    init_sdl();

    img = load_image(path);

    grayscale(img);

    bin = thresholding(img, moy_img(img));
    *binary = bin;

    GaussianBlur(img);

    img = gradient(img);

    return img;


    /*if(SDL_SaveBMP(img, "result.bmp") != 0)
    {
        // Error saving bitmap
        printf("SDL_SaveBMP failed: %s\n", SDL_GetError());
    }
    else{
        printf("saved");
    }
    SDL_FreeSurface(img);
    SDL_Quit();*/

}


#ifndef NeuralNetwork_H
#define NeuralNetwork_H

#include <math.h>
#include "ToolsAI.h"


struct NeuralNetwork
{
    // Structure of network : Specifications
    int nbI;
    int nbH;
    int nbO;
    double LR;
    double Temp;

    //Structure of network : Storage of network
    // Matrix value of the hidden layer
    struct Matrix HV;
    // Board to keep value of output layer
    struct Matrix OV; //to init
    // Matrix of weights between input neurons to hidden neurons
    struct Matrix WeightsIH;
    // Matrix of weights between hidden neurons to output neurons
    struct Matrix WeightsHO;
    // Matrix of the biases of hidden neurons
    struct Matrix BH;
    // Matrix to keep bias of output neurons
    struct Matrix BO;

    //Forward
    struct Matrix SumHO;

    //BackPropagation
    struct Matrix SumIH;

    //Debug 1
    struct Matrix DOutput;
    struct Matrix DWHO;
    struct Matrix DHidden;
    struct Matrix DWIH;

    struct Matrix pass;

};

struct NeuralNetwork InitializeNetwork();

void ImportNetwork(const char* WIH, const char* WHO,
                   const char* BH, const char* BO,
                   struct NeuralNetwork network);

void TrainingNeuralNetwork(const char* train);

void TestNeuralNetwork(const char* exam);

int RecognizeDigit(SDL_Surface* img, struct NeuralNetwork network);


#endif
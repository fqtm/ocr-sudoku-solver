#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <SDL/SDL_rotozoom.h>
#include "Grid_Detection.h"
#include "ImageProc.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

int iround(double value) {
    return floor(value + 0.5);
}

struct matrix{
    int row;
    int col;
    int* M;
};

int get_matrix_cell(struct matrix* M, int i, int j){
    int col = M->col;
    return M->M[i * col + j];
}

void put_matrix_cell(struct matrix* M, int i, int j, int x){
    int col = M->col;
    M->M[i * col + j] = x;
}

void add_matrix_cell(struct matrix* M, int i, int j, int x){
    int col = M->col;
    M->M[i * col + j] += x;
}

void and_matrix(struct matrix* m1, struct matrix* m2){
    ///applies and operator on m1 matrix
    int col = m1->col;
    int row = m1->row;
    if (col != m2->col || row != m2->row) {
        fprintf(stdout, "and_matrix: matrix of to different sizes");
    }
    for (int i = 0; i < row * col; i++){
        m1->M[i] = m1->M[i] & m2->M[i];
    }
}

void or_matrix(struct matrix* m1, struct matrix* m2){
    ///applies or operator on m1 matrix
    int col = m1->col;
    int row = m1->row;
    if (col != m2->col || row != m2->row) {
        fprintf(stdout, "and_matrix: matrix of to different sizes");
    }
    for (int i = 0; i < row * col; i++){
        m1->M[i] = m1->M[i] | m2->M[i];
    }
}

void draw_line_blue(SDL_Surface* img, struct line* line){
    int w = img->w;
    int h = img->h;

    int rho = line->rho;
    int theta = line->theta;

    Uint8 r, g, b;
    Uint32 blue_pixel = SDL_MapRGB(img->format, 0, 0, 255);
    Uint32 pixel;

    double PI = 3.14159;
    double rad = (double)theta * (PI/180.0);

    if ((theta >= 45 && theta < 135) || (theta >= 225 && theta < 315)){ //better to calculate Y for "middle" angles
        int y;
        double sinus = sin(rad);
        for (int x = 0; x < w; x++) {
            y = iround(((double)rho - (double)x * cos(rad))/sinus);
            if (y >= 0 && y < h){
                pixel = get_pixel(img, x, y);
                SDL_GetRGB(pixel, img->format, &r, &g, &b);
                put_pixel(img, x, y, blue_pixel);
            }
        }
    }
    else{
        int x;
        double cosinus = cos(rad);
        for (int y = 0; y < h; y++) {
            x = iround(((double)rho - (double)y * sin(rad))/cosinus);
            if (x >= 0 && x < w){
                pixel = get_pixel(img, x, y);
                SDL_GetRGB(pixel, img->format, &r, &g, &b);
                put_pixel(img, x, y, blue_pixel);
            }
        }
    }
}

void draw_line_green(SDL_Surface* img, struct line* line){
    int w = img->w;
    int h = img->h;

    int rho = line->rho;
    int theta = line->theta;

    Uint8 r, g, b;
    Uint32 green_pixel = SDL_MapRGB(img->format, 0, 255, 0);
    Uint32 pixel;

    double PI = 3.14159;
    double rad = (double)theta * (PI/180.0);

    if ((theta >= 45 && theta < 135) || (theta >= 225 && theta < 315)){ //better to calculate Y for "middle" angles
        int y;
        double sinus = sin(rad);
        for (int x = 0; x < w; x++) {
            y = iround(((double)rho - (double)x * cos(rad))/sinus);
            if (y >= 0 && y < h){
                pixel = get_pixel(img, x, y);
                SDL_GetRGB(pixel, img->format, &r, &g, &b);
                put_pixel(img, x, y, green_pixel);
            }
        }
    }
    else{
        int x;
        double cosinus = cos(rad);
        for (int y = 0; y < h; y++) {
            x = iround(((double)rho - (double)y * sin(rad))/cosinus);
            if (x >= 0 && x < w){
                pixel = get_pixel(img, x, y);
                SDL_GetRGB(pixel, img->format, &r, &g, &b);
                put_pixel(img, x, y, green_pixel);
            }
        }
    }
}

void RLSA(SDL_Surface* img, int threshold){
    int w = img->w;
    int h = img->h;
    Uint32 pixel;
    Uint8 r, g, b;

    //lines and columns separated RLSA
    struct matrix lines_RLSA = {h, w, malloc(w * h * sizeof(int))};
    struct matrix columns_RLSA = {h, w, malloc(w * h * sizeof(int))};


    int nb_0 = 0;

    //creating lines RLSA
    for (int y = 0; y < h; y++){
        for (int x = 0; x < w; x++){
            pixel = get_pixel(img, x, y);
            SDL_GetRGB(pixel, img->format, &r, &g, &b);
            put_matrix_cell(&lines_RLSA, x, y, r/255);
            if (r == 0){
                nb_0++;
            }
            else {
                if (nb_0 <= threshold){ //"collapsing" 1's
                    for (int n = nb_0; n > 0; n--){
                        put_matrix_cell(&lines_RLSA, x-n, y, 1);
                    }
                }
                nb_0 = 0;
            }
        }
    }
    nb_0 = 0;

    //creating columns RLSA
    for (int x = 0; x < w; x++){
        for (int y = 0; y < h; y++){
            pixel = get_pixel(img, x, y);
            SDL_GetRGB(pixel, img->format, &r, &g, &b);
            put_matrix_cell(&columns_RLSA, x, y, r/255);
            if (r == 0){
                nb_0++;
            }
            else {
                if (nb_0 <= threshold){
                    for (int n = nb_0; n > 0; n--){
                        put_matrix_cell(&columns_RLSA, x, y-n, 1);
                    }
                }
                nb_0 = 0;
            }
        }
    }

    and_matrix(&lines_RLSA, &columns_RLSA);

    //Redraw picture
    Uint32 matrix_pixel;
    for (int x = 0; x < w; x++){
        for (int y = 0; y < h; y++){
            matrix_pixel = get_matrix_cell(&lines_RLSA, x, y) * 255;
            pixel = SDL_MapRGB(img->format, matrix_pixel, matrix_pixel, matrix_pixel);
            put_pixel(img, x, y, pixel);
        }
    }
}

struct line* hough_transform(SDL_Surface* img, size_t* nblines){
    struct line* lines = NULL;
    *nblines = 0;
    int w = img->w;
    int h = img->h;
    double PI = 3.14159;

    Uint32 pixel;
    Uint8 r, g, b;
    int delta = 1;
    int max_rho = ceil(sqrt(w * w + h * h));
    int rho;
    int* M = malloc(max_rho * (360 / delta) * sizeof(int));

    //Matrix size m[max_rho][180/delta]
    struct matrix m = {max_rho, 360/delta, M};

    //Matrix initialization to 0
    for (int i = 0; i < max_rho; i++){
        for (int j = 0; j < 360 / delta; j++) {
            put_matrix_cell(&m, i, j, 0);
        }
    }

    //Get all lines for all contours
    double rad;
    for (int x = 0; x < w ; x++) {
        for (int y = 0; y < h ; y++) {
            pixel = get_pixel(img, x, y);
            SDL_GetRGB(pixel, img->format, &r, &g, &b);

            if (r == 255){
                for (int theta = 0; theta < 180; theta += delta) {
                    rad = (double)theta * (PI/180.0);
                    rho = (int)((double)x * cos(rad) + (double)y * sin(rad));
                    if (rho < 0){
                        add_matrix_cell(&m, -rho, (180 + theta)/delta, 1);
                    }
                    else{
                        add_matrix_cell(&m, rho, theta/delta, 1);
                    }

                }
            }
        }
    }

    //Get max in matrix
    int max = 0;
    int cell;
    for (int i = 0; i < max_rho; i++){
        for (int j = 0; j < 360 / delta; j++){
            cell = get_matrix_cell(&m, i, j);
            if (max < cell){
                max = cell;
            }
        }
    }

    //Get all lines form maxes in matrix
    struct line newline;
    int threshold = 0.65 * (double)max;
    for (int i = 0; i < max_rho; i++){
        for (int j = 0; j < 360 / delta; j++){
            cell = get_matrix_cell(&m, i, j);
            if (threshold < cell){
                (*nblines)++;
                newline = (struct line){i, j * delta};
                lines = (struct line*) realloc(lines, (*nblines) * sizeof(struct line));
                lines[(*nblines) - 1] = newline;
            }
        }
    }
    return lines;
}

struct line* clean_lines(struct line* lines, size_t* nb_lines){
    //Removes double lines (lines close enough)
    struct line line1;
    struct line line2;

    struct line* new_lines = NULL;
    size_t new_nb_lines = 0;

    int theta1;
    int theta2;
    int rho1;
    int rho2;

    int accepted;

    for (size_t i = 0; i < *nb_lines; i++){
        accepted = 1;
        line1 = lines[i];
        theta1 = line1.theta;
        rho1 = line1.rho;
        for (size_t j = 0; j < new_nb_lines; j++){

            line2 = new_lines[j];
            theta2 = line2.theta;
            rho2 = line2.rho;
            if (abs(theta1 - theta2) < 3 || (abs(theta1 - theta2) > 177 && abs(theta1 - theta2) < 183)){
                if (abs(rho1 - rho2) < 5){
                    accepted = 0;
                    break;
                }
            }
        }
        if (accepted){
            new_nb_lines++;
            new_lines = realloc(new_lines, new_nb_lines * sizeof(struct line));
            new_lines[new_nb_lines - 1] = line1;
            //draw_line_blue(img, &line1);
        }
    }
    *nb_lines = new_nb_lines;
    return new_lines;
}

struct line* get_sudoku_lines(SDL_Surface* img, struct line* lines, size_t* nb_lines){
    ///sort lines with a theta differential of 90 degrees
    ///only keep the largest group of lines
    int* theta_vector = calloc(90, sizeof(int));

    //accumulate thetas
    for (size_t i = 0; i < *nb_lines; i++){
        theta_vector[lines[i].theta % 90]++;
    }

    int max = 0;
    int max_index = 0;
    //get max theta in the accumulation vector
    for (size_t i = 0; i < 90; i++){
        if (theta_vector[i] > max){
            max = theta_vector[i];
            max_index = i;
        }
    }
    struct line* new_lines = NULL;
    size_t nb_new_lines = 0;
    //keep lines with a theta similar to max (+-)5
    for (size_t i = 0; i < *nb_lines; i++){
        if ((lines[i].theta <= max_index + 3 &&
        lines[i].theta >= max_index - 3)
        ||
        (lines[i].theta <= max_index + 93 &&
        lines[i].theta >= max_index + 87)
        ||
        (lines[i].theta <= max_index + 183 &&
        lines[i].theta >= max_index + 177)
        ||
        (lines[i].theta <= max_index + 273 &&
         lines[i].theta >= max_index + 267)){
            nb_new_lines++;
            new_lines = realloc(new_lines, nb_new_lines * sizeof(struct line));
            new_lines[nb_new_lines - 1] = lines[i];
        }
    }
    *nb_lines = nb_new_lines;
    new_lines = clean_lines(new_lines, nb_lines);
    return new_lines;

}

void draw_point_light_blue(SDL_Surface* img, struct couple* point){
    Uint32 pixel = SDL_MapRGB(img->format, 0, 255, 255);
    put_pixel(img, point->x, point->y, pixel);
}

void draw_intersects(SDL_Surface* img, struct line* lines, size_t nblines){
    struct couple* points = NULL;
    size_t nbpoints = 0;
    double a1, a2, b1, b2; //y = ax + b or x = ay + b
    double PI = 3.14159;
    double rad1, rad2;
    int theta1, rho1, theta2, rho2;
    struct couple point;
    int w = img->w;
    int h = img->h;


    for (size_t i = 0; i < nblines; i++){
        theta1 = lines[i].theta;
        rho1 = lines[i].rho;
        rad1 = (double)theta1 * PI/180.0;
        for (size_t j = i + 1; j < nblines; j++){
            theta2 = lines[j].theta;
            rho2 = lines[j].rho;
            rad2 = (double)theta2 * PI/180.0;

            if ((abs(theta1 - theta2) <= 5) ||
            (abs(theta1 - theta2) <= 185 && abs(theta1 - theta2) >= 175)) {// parallels lines
                continue;
            }

            if ((theta1 >= 45 && theta1 < 135) || (theta1 >= 225 && theta1 < 315)){
                //y = ax + b
                a1 = -cos(rad1)/sin(rad1);
                a2 = -cos(rad2)/sin(rad2);
                b1 = rho1/sin(rad1);
                b2 = rho2/sin(rad2);

                if (fabs(a1) < 0.0001){
                    point = (struct couple){rho1, rho2};
                }
                else
                    point = (struct couple){iround((b2 - b1)/(a1 - a2)), iround(a1 * (b2-b1) / (a1 - a2) + b1)};
            }
            else{
                //x = ay + b
                a1 = -sin(rad1)/cos(rad1);
                a2 = -sin(rad2)/cos(rad2);
                b1 = rho1/cos(rad1);
                b2 = rho2/cos(rad2);
                if (fabs(a1) < 0.0001){
                    point = (struct couple){rho2, rho1};
                }
                else
                    point = (struct couple) {iround(a1 * (b2-b1) / (a1 - a2) + b1), iround(b2 - b1)/(a1 - a2)};
            }

            if (point.x > 0 && point.y > 0 && point.x < w && point.y < h){//safety
                nbpoints++;
                points = realloc(points, nbpoints * sizeof(struct couple));
                points[nbpoints - 1] = point;
                draw_point_light_blue(img, &point);
            }
        }
    }
}

int is_on_line(SDL_Surface* img, struct couple point1, struct couple point2){
    int sum_white = 0, sum_black = 0;
    Uint32 pixel;
    Uint8 r,g,b;
    if (abs(point1.y - point2.y) < 2){
        for (int x = point1.x + 1; x < point2.x; x++){
            pixel = get_pixel(img, x, point1.y);
            SDL_GetRGB(pixel, img->format, &r,&g,&b);
            if (r == 0){
                sum_black++;
            }
            else{
                sum_white++;
            }
        }
    }
    else if (abs(point1.x - point2.x) < 2){
        for (int y = point1.y + 1; y < point2.y; y++){
            pixel = get_pixel(img,point1.x, y);
            SDL_GetRGB(pixel, img->format, &r,&g,&b);
            if (r == 0){
                sum_black++;
            }
            else{
                sum_white++;
            }
        }
    }
    return sum_white > 1.2 * sum_black;

}

void make_points_group(SDL_Surface* img, struct couple* points, size_t i, size_t nbpoints, int* groups, int group_number){
    groups[i] = group_number;
    for (size_t j = 0; j < nbpoints; j++){
        if (i != j && groups[j] == 0){
            if (is_on_line(img, points[i], points[j])){
                make_points_group(img, points, j, nbpoints, groups, group_number);
            }
        }
    }
}

struct couple* get_greater_polygon_corners(SDL_Surface* img,
        struct line* lines, size_t nblines, int* theta_rot){
    draw_intersects(img, lines, nblines);
    int min_theta = 360;
    for (size_t i = 0; i < nblines; i++) {
        if (lines[i].theta < min_theta) {
            min_theta = lines[i].theta;
        }
    }
    SDL_Surface* new_img = img;
    if (min_theta != 0){
        new_img = rotozoomSurface(img, min_theta, 1, 0);
    }

    *theta_rot = min_theta;
    int w = new_img->w;
    int h = new_img->h;

    Uint32 pixel;
    Uint8 r, g, b;

    struct couple* points = NULL;
    size_t nbpoints = 0;

    for (int x = 0; x < w; x++){ //Get all light_blue intersection pixels
        for (int y = 0; y < h; y++){
            pixel = get_pixel(new_img, x, y);
            SDL_GetRGB(pixel, new_img->format, &r, &g, &b);
            if (g == 255 && b == 255 && r == 0){
                nbpoints++;
                points = realloc(points, nbpoints * sizeof(struct couple));
                points[nbpoints - 1] = (struct couple) {x,y};
            }
        }
    }


    int* groups = calloc(nbpoints, sizeof(int));
    int groups_number = 1;

    for (size_t i = 0; i < nbpoints; i++){
        if (groups[i] == 0){
            make_points_group(new_img, points, i, nbpoints, groups, groups_number);
            groups_number++;
        }
    }

    int* groups_size = calloc(groups_number, sizeof(int));
    for (size_t i = 0; i < nbpoints; i++){
        groups_size[groups[i]]++;
    }
    size_t max_group = 1;
    int max = -1;
    for (int i = 0; i < groups_number; i++){
        if (groups_size[i] > max){
            max = groups_size[i];
            max_group = i;
        }
    }

    struct couple* new_points = malloc(max * sizeof(struct couple));
    size_t new_point_index = 0;

    for (size_t i = 0; i < nbpoints; i++){
        if (groups[i] == (int)max_group){
            new_points[new_point_index] = points[i];
            new_point_index++;
        }
    }

    int min_x = new_points[0].x, min_y = new_points[0].y, max_x = new_points[max -1].x, max_y = new_points[max -1].y;


    struct couple *corners = malloc(4 * sizeof(struct  couple));
    corners[0] = (struct couple) {min_x, min_y};
    corners[1] = (struct couple) {min_x, max_y};
    corners[2] = (struct couple) {max_x, min_y};
    corners[3] = (struct couple) {max_x, max_y};

    return corners;
}

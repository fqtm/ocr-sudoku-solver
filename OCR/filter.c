//
// Created by maxen on 18/10/2021.
//

#include "filter.h"
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include "ImageProc.h"

#include <stdio.h>
#include <math.h>
#include <float.h>



void grayscale(SDL_Surface* image_surface) {


    int width = image_surface->w;
    int height = image_surface->h;
    for(int x = 0  ; x < width  ; x++) {
        for(int y = 0; y < height ; y++){
            Uint32 pixel = get_pixel(image_surface, x , y);
            Uint8 r, g, b;
            SDL_GetRGB(pixel, image_surface->format,&r , &g, &b);
            int average =  0.3*r + 0.59*g+0.11*b;
            r = g = b = average;
            Uint32 new_pixel = SDL_MapRGB(image_surface->format, r, g, b);
            put_pixel(image_surface, x , y , new_pixel);
        }
    }

}

Uint32 moyennepixel(SDL_Surface* surface, int i, int j, int n)
{
    const int initial_h = SDL_max(i - n, 0);
    const int initial_w = SDL_max(j - n, 0);
    const int final_h = SDL_min(i + n, surface->h -1 );
    const int final_w = SDL_min(j + n, surface->w -1 );
    const int nb_pixel = ((final_h - initial_h) * (final_w - initial_w));
    Uint32 sum_r = 0, sum_g = 0, sum_b = 0;
    Uint8 r,g,b;

    for (int x = initial_w; x < final_w; x++) {
        for (int y = initial_h; y < final_h; y++) {
            Uint32 pixel = get_pixel(surface, x, y);

            SDL_GetRGB(pixel,surface->format,&r,&g,&b);
            sum_r += r;
            sum_g += g;
            sum_b += b;

        }

    }
    return SDL_MapRGB(surface->format, sum_r / nb_pixel, sum_g / nb_pixel,
                      sum_b / nb_pixel);
}

void GaussianBlur(SDL_Surface* image_surface) {

    int width = image_surface->w;
    int height = image_surface->h;
    for(int x = 0  ; x < width   ; x++) {
        for(int y = 0; y < height  ; y++){
            const Uint32 newpixel = moyennepixel(image_surface,y,x,3);
            put_pixel(image_surface, x , y , newpixel );
        }

    }
}

int convolution(int matrix[3][3],int x) {
    int rows = 3;
    int cols = 3;
    int sum = 0;
    int GX[3][3] =
            {
                    {-1,0,1},
                    {-2,0,2},
                    {-1,0,1}
            };
    int GY[3][3] =
            {
                    {-1,-2,-1},
                    {0,0,0},
                    {1,2,1}
            };
    //Check if we will do a convolution with x ( x = 1) or y ( x = 0)
    if (x == 1)
    {
        for (int i = 0; i < rows; ++i) {

            for (int j = 0; j < cols; ++j) {
                //Convolution mat with GX
                sum += matrix[i][j] * GX[i][j];
            }
        }
    }
    else {
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                //Convolution mat with GY
                sum += matrix[i][j] * GY[i][j];
            }
        }
    }

    return sum;//return the value of the pixel
}

SDL_Surface* gradient(SDL_Surface* image_surface) {
    int width = image_surface->w;
    int height = image_surface->h;

    SDL_Surface* image = SDL_CreateRGBSurface(0,width,height,32,0,0,0,0);

    

    //2 loop to treat each pixel
    for (int x = 1; x < width-1; ++x) {
        for (int y = 1; y < height-1 ; ++y) {
            int mat[3][3] ;

            //Creation of my mat 3*3, i put a pixel in a part of the mat
            for (int i = -1; i < 2 ; ++i) {

                for (int j = -1; j <2 ; ++j) {
                    Uint32 pixel = get_pixel(image_surface, x + i, y +j);
                    Uint8 r, g, b;
                    SDL_GetRGB(pixel, image_surface->format,&r , &g, &b);
                    mat[j+1][i+1] = (int) r;
                }
                   }
            Uint32 pixGX = (Uint32) convolution(mat,1);
            Uint32 pixGY = (Uint32) convolution(mat,0);


            int gradient = sqrt((pixGX*pixGX) + (pixGY*pixGY));

            if (gradient > 50){

                put_pixel(image,x,y ,
                          SDL_MapRGB(image->format,255,255,255) );
            }
            else  {
                put_pixel(image,x,y,SDL_MapRGB(image->format,0,0,0));
            }
        }

    }
    return image;
}


int moy_img(SDL_Surface* image) {
    int w = image->w;
    int h = image->h;
    Uint32 pixel;
    Uint8 r, g, b;
    int sum = 0;
    for (int x = 0; x < w ; x++) {
        for (int y = 0; y < h; y++) {
            pixel = get_pixel(image, x, y);
            SDL_GetRGB(pixel, image->format, &r, &g, &b);
            sum += r;
        }
    }
    return sum/(h*w);
}



SDL_Surface* thresholding(SDL_Surface* img, int threshold){
    int w = img->w;
    int h = img->h;
    Uint32 pixel;
    Uint8 r, g, b;
    SDL_Surface *thresholded = SDL_CreateRGBSurface(0,w,h,32,0,0,0,0);

    for (int x = 0; x < w ; x++) {
        for (int y = 0; y < h; y++) {
            pixel = get_pixel(img, x, y);
            SDL_GetRGB(pixel, img->format, &r, &g, &b);
            if (r > threshold){
                pixel = SDL_MapRGB(thresholded->format, 255, 255, 255);
            }
            else
            {
                pixel = SDL_MapRGB(thresholded->format, 0, 0, 0);
            }
            put_pixel(thresholded, x, y, pixel);
        }
    }
    return thresholded;
}







#ifndef TOOLSAI_H_
#define TOOLSAI_H_

#include "ImageProc.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>



/*##############################*/
/*          _Matrices_          */
/*##############################*/

// Structure declaration
struct Matrix{

    int rows;
    int columns;
    double *mat;
};

//Initialization
struct Matrix CreateMatrix(int rows, int colums);
void InitMatrixZero(struct Matrix matrix);
void InitRandomMatrix(struct Matrix matrix);

// Get and Set
double GetMatrix(struct Matrix matrix, int x, int y);
void SetMatrix(struct Matrix matrix, int x, int y, float val);

// Test and Debug
int IsCoorValid(struct Matrix matrix, int x, int y);
void PrintMatrix(struct Matrix matrix);
double DebugGet(struct Matrix matrix, int x, int y);
void DebugSet(struct Matrix matrix, int x, int y, float val);

/*##############################*/
/*          _Activation_        */
/*##############################*/

double Random();

double Relu(double x);

double dRelu(double x);

double Softmax(struct Matrix m, int o, int nbo, double Temp);

/*##############################*/
/*          _Functions_         */
/*##############################*/

int Max(struct Matrix outputs);

void Delete0(const char* Source, const char* Dest);

void thresholdingCSV(const char* source, const char* dest);

char threshold(int value);

/*##############################*/
/*       _Images and Data_      */
/*##############################*/

struct LabeledData{
    double label;
    struct Matrix matpix;
    double onehot[9];
};

struct LabeledData CreateLD(int l, struct Matrix m);

void SetMatrixFromLine(char* line, struct Matrix matpix);

void TestSetFromLine(const char* src);

void SetMatFromImg(struct Matrix matpix, SDL_Surface* image_surface);

int is_black(SDL_Surface* img);

#endif
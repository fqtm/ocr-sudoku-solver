
#ifndef OCR_SUDOKU_SOLVER_GRID_GETTER_H
#define OCR_SUDOKU_SOLVER_GRID_GETTER_H

SDL_Surface *Get_cells(SDL_Surface *img, SDL_Surface* binary);

#endif //OCR_SUDOKU_SOLVER_GRID_GETTER_H

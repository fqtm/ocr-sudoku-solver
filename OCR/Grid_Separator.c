#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Grid_Separator.h"
#include "Grid_Detection.h"
#include "ImageProc.h"
#include <SDL/SDL_rotozoom.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

void separate_grid(SDL_Surface* img, SDL_Surface* cells[81], int x_min, int y_min, int x_max, int y_max){
    printf("min = %d;%d, max = %d;%d\n", x_min, y_min, x_max, y_max);
    int w = x_max + 1 - x_min;
    int h = y_max + 1 - y_min;
    int dim = w > h ? w : h; //Reduce the picture to a square
    int cell_dim = iround(dim / 9);
    double zoom;

    Uint8 r, g, b;
    SDL_Surface* cell = NULL; //new img
    Uint32 pixel;
    get_pixel(img, 0, 0);

    for (int row = 0; row < 9; row++){
        for (int col = 0; col < 9; col++){//iterates for each row/cols in the sudoku
            cell = SDL_CreateRGBSurface(0,cell_dim,cell_dim,32,0,0,0,0);
            for (int x = x_min; x < cell_dim + x_min; x++){//iterates for every pixel in the cell
                for (int y = y_min; y < cell_dim + y_min; y++){
                    pixel = get_pixel(img, x + row * cell_dim,
                                      y + col * cell_dim);
                    SDL_GetRGB(pixel, img->format, &r, &g, &b);
                    if (r > 0)
                        pixel = SDL_MapRGB(img->format, 255, 255, 255);
                    else
                        pixel = SDL_MapRGB(img->format, 0, 0, 0);
                    //Gets pixel depending on which row and col it's iterating
                    put_pixel(cell, x - x_min, y - y_min, pixel);
                }
            }
            //img resizing

            zoom = 28.0 / (double) cell_dim; //Clean
            cell = rotozoomSurface(cell, 0, zoom, 1);
            for (int x = 0; x < 28; x++){
                pixel = SDL_MapRGB(img->format, 0, 0, 0);
                put_pixel(cell, x, 0, pixel);
                put_pixel(cell, x, 1, pixel);
                put_pixel(cell, x, 26, pixel);
                put_pixel(cell, x, 27, pixel);
            }
            for (int x = 1; x < 27; x++){
                put_pixel(cell, 0, x, pixel);
                put_pixel(cell, 1, x, pixel);
                put_pixel(cell, 26, x, pixel);
                put_pixel(cell, 27, x, pixel);
            }

            //accessing array like a matrix
            cells[row + 9 * col] = cell;
        }
    }

}

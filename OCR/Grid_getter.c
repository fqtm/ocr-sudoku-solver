#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Grid_Detection.h"
#include "Grid_Separator.h"
#include "Grid_getter.h"
#include "ImageProc.h"
#include "filter.h"
#include <stdio.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>


SDL_Surface *Get_cells(SDL_Surface *img, SDL_Surface *binary){
    size_t nblines;

    struct line* lines = hough_transform(img, &nblines);

    lines = get_sudoku_lines(img, lines, &nblines);

    int theta;
    struct couple* corners = get_greater_polygon_corners(img, lines, nblines, &theta);

    binary = rotozoomSurface(binary, theta, 1, 0);
    binary = thresholding(binary, moy_img(binary));

    int max_x = corners[3].x, max_y = corners[3].y, min_x = corners[0].x, min_y = corners[0].y;
    SDL_Surface cells[81];

    separate_grid(binary, cells, min_x, min_y, max_x, max_y);
    return cells;
}
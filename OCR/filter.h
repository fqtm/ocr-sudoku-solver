//
// Created by maxen on 18/10/2021.
//

#include <SDL.h>
#ifndef OCR_SUDOKU_SOLVER_FILTER_H
#define OCR_SUDOKU_SOLVER_FILTER_H

void grayscale(SDL_Surface* image_surface);
void Black_White_filter(SDL_Surface* image_surface);
Uint32 moyennepixel(SDL_Surface *surface, int i, int j, int n);
void GaussianBlur(SDL_Surface* image_surface);
int convolution(int matrix[3][3],int x);
SDL_Surface* gradient(SDL_Surface* image_surface);
int moy_img(SDL_Surface* image);
SDL_Surface* thresholding(SDL_Surface* img, int threshold);

#endif //OCR_SUDOKU_SOLVER_FILTER_H




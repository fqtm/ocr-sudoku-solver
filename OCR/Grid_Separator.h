#ifndef OCR_SUDOKU_SOLVER_GRID_SEPARATOR_H
#define OCR_SUDOKU_SOLVER_GRID_SEPARATOR_H

void separate_grid(SDL_Surface* img, SDL_Surface* cells[81], int x_min, int y_min, int x_max, int y_max);

#endif //OCR_SUDOKU_SOLVER_GRID_SEPARATOR_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Grid_Separator.h"
#include "ImageProc.h"
#include <stdio.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>

int main(){
    SDL_Surface* img;
    freopen("stdout.txt", "w",stdout);
    init_sdl();
    printf("\n");

    img = load_image("img/sudoku_contoured_easy.png");
    SDL_Surface* cells[81];
    separate_grid(img, cells);

    SDL_Surface* cell;
    char result[] = "result00.bmp";
    for (int i = 0; i < 81; i++){
        cell = cells[i];
        result[6] = (char)(i / 10 + '0');
        result[7] = (char)(i % 10 + '0');
        if (SDL_SaveBMP(cell, result) != 0)
        {
            // Error saving bitmap
            printf("SDL_SaveBMP failed: %s\n", SDL_GetError());
        }
        else{
            printf("saved");
        }
        SDL_FreeSurface(cell);
    }



    SDL_FreeSurface(img);
    SDL_Quit();
}

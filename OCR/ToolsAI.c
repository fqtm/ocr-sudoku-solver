#include "ToolsAI.h"

/*##############################*/
/*          _Matrices_          */
/*##############################*/

//Initialization
struct Matrix CreateMatrix(int rows, int columns){

    struct Matrix matrix;

    matrix.rows = rows;
    matrix.columns = columns;
    matrix.mat = malloc(sizeof(double)*columns*rows);

    return matrix;
}

void InitMatrixZero(struct Matrix matrix){
    for (int row = 0; row < matrix.rows; row++){
        for(int col = 0; col < matrix.columns; col++){
            SetMatrix(matrix, row, col, 0);
        }
    }
}

void InitRandomMatrix(struct Matrix matrix){
    for (int row = 0; row < matrix.rows; row++){
        for(int col = 0; col < matrix.columns; col++){
            SetMatrix(matrix, row, col, Random());
        }
    }
}

// Get and Set
double GetMatrix(struct Matrix matrix, int row, int col){
    if (!IsCoorValid(matrix, row, col))
        printf("Error Coor Get\n");
    return *(matrix.mat + row * matrix.columns + col);
}

void SetMatrix(struct Matrix matrix, int row, int col, float val){
    if (!IsCoorValid(matrix, row, col))
        printf("Error Coor Set\n");
    *(matrix.mat + row * matrix.columns + col) = val;
}

//Test and Debug
int IsCoorValid(struct Matrix matrix, int row, int col){
    return (row < matrix.rows &&  col < matrix.columns);
}

void PrintMatrix(struct Matrix matrix){
    for (int row = 0; row < matrix.rows; row++){
        for(int col = 0; col < matrix.columns; col++){
            printf("%f ", GetMatrix(matrix, row, col));
        }
        printf("\n");
    }
}

double DebugGet(struct Matrix matrix, int row, int col){
    if (!IsCoorValid(matrix, row, col))
        printf("It is me which isn't working : GET\n");
    return *(matrix.mat + row * matrix.columns +col);
}

void DebugSet(struct Matrix matrix, int row, int col, float val){
    if (!IsCoorValid(matrix, row, col))
        printf("It is me which isn't working : SET\n");
    *(matrix.mat + row * matrix.columns + col) = val;
}

/*##############################*/
/*          _Maths_             */
/*##############################*/

double Random(){
    return (double)rand()/(double)RAND_MAX;
}

double Relu(double x){
    if(x<=0)
        return 0;
    return x;
}

double dRelu(double x){
    if(x<0)
        return 0;
    return 1;
}

double Softmax(struct Matrix m, int o, int nbo, double Temp){
    /*double b[9];
    for (int i = 0; i < nbo; ++i) {
        b[i]=GetMatrix(m,i,0);
    }
    double maxv= 0;
    for (int i = 0; i < nbo; ++i) {
       if(b[i]>maxv){
           maxv = b[i];
       }
    }
    for (int i = 0; i < nbo; ++i) {
        b[i]= b[i]-maxv;
    }*/

    double denominator = 0;
    for (int i = 0; i < nbo; ++i) {
        denominator += exp(GetMatrix(m,i,0)/Temp);
    }
    double numerator = exp(GetMatrix(m,o,0)/Temp);
    double  r = numerator / denominator;
    return r;
}

/*##############################*/
/*          _Functions_         */
/*##############################*/

int Max(struct Matrix output){
    double max = 0;
    int index = 0;
    for (int i = 0; i < 9 ; ++i){
        double current = GetMatrix(output,i,0);
        if(current >= max){
            max = current;
            index = i;
        }
    }
    return index+1;
}

void Delete0(const char* source, const char* dest){
    FILE* src = fopen(source, "r");
    FILE* dst = fopen(dest, "w");
    if(src == NULL){
        printf("Impossible to open src");
        return;
    }
    int is_label = 1;
    int c = fgetc(src);
    while(c != EOF){
        if(is_label == 1 && c == '0'){
            while (c != '\n'){
                c = fgetc(src);
            }
            c = fgetc(src);
        }
        else{
            if(is_label==1){
                is_label = 0;
            }
            if(c == '\n'){
                is_label = 1;
            }
            fputc(c, dst);
            c = fgetc(src);
        }
    }
    fclose(src);
    fclose(dst);
}

void thresholdingCSV(const char* source, const char* dest){
    FILE* src = fopen(source, "r");
    FILE* dst = fopen(dest, "w");
    if(src == NULL){
        printf("Impossible to open src");
        return;
    }
    int c = fgetc(src);
    int v = 0;
    int is_label = 1;
    while(c != EOF){
        if(c == ',' || c == '\n'){
            fputc(threshold(v), dst);
            fputc(c, dst);
            v = 0;
            if(c == '\n'){
                is_label = 1;
            }
        }
        else if(is_label== 1){
            fputc(c, dst);
            is_label = 0;
            c = fgetc(src);
            fputc(c, dst);
        }
        else{
            v = v * 10 + (c - '0');
        }
        c = fgetc(src);
    }
    fclose(src);
    fclose(dst);
}

char threshold(int v){
    if(v < 100)
        return '0';
    return '1';
}

/*##############################*/
/*       _Images and Data_      */
/*##############################*/

void SetMatrixFromLine(char* line, struct Matrix matpix) {
    double *p = matpix.mat;
    for (size_t i = 1; i < 785; i++) {
        *p = ((double)((*(line + i)) - '0'));
        p++;
    }
}

void TestSetFromLine(const char* source){
    FILE* src = fopen(source, "r");
    if(src == NULL){
        printf("Impossible to open src in TestSetFromLine");
        return;
    }
    struct Matrix t = CreateMatrix(784,1);
    char line[788];
    fgets(line, 788, src);
    SetMatrixFromLine(line, t);
    PrintMatrix(t);
    fclose(src);
}

struct LabeledData CreateLD(int l, struct Matrix m){

    struct LabeledData ld;

    ld.label = ((double)l);
    ld.matpix = m;
    for (int i = 0; i < 9; ++i) {
        ld.onehot[i] = 0;
    }
    ld.onehot[l-1] = 1;

    return ld;
}

void SetMatFromImg(struct Matrix matpix, SDL_Surface* image_surface){
    for(int y = 0  ; y < 28  ; y++) {
        for(int x = 0; x < 28 ; x++){
            Uint32 pixel = get_pixel(image_surface, x , y);
            Uint8 r, g, b;
            SDL_GetRGB(pixel, image_surface->format,&r , &g, &b);
            SetMatrix(matpix,x + 28 * y ,0,((double) r/255.0));
        }
    }
}

int is_black(SDL_Surface* img){
    int w = img->w;
    int h = img->h;
    Uint32 pixel;
    Uint8 r, g, b;
    int sumb = 0;
    for (int x = 0; x < w ; x++) {
        for (int y = 0; y < h; y++) {
            pixel = get_pixel(img, x, y);
            SDL_GetRGB(pixel, img->format, &r, &g, &b);
            if (r < 10) {
                sumb++;
            }
        }
    }
    return (sumb > 784*0.9);
}





#include "Solver.h"
#include <stdio.h>
#include <gtk/gtk.h>



int CanAdd(size_t grid[][9], size_t row, size_t col, size_t n)
{
    size_t rowStart = (row/3) * 3;
    size_t colStart = (col/3) * 3;
    size_t i;

    for(i=0; i<9; ++i)
    {
        if (grid[row][i] == n)
            return 0;
        if (grid[i][col] == n)
            return 0;
        if (grid[rowStart + (i%3)][colStart + (i/3)] == n)
            return 0;
    }
    return 1;
}

int SudoSolver(size_t grid[][9],size_t row, size_t col)
{

    if(row<9 && col<9)
    {
        if(grid[row][col] != 0)
        {
            if((col+1)<9) return SudoSolver(grid, row, col+1);
            else if((row+1)<9) return SudoSolver(grid, row+1, 0);
            else return 1;
        }
        else
        {
            for(size_t i = 1 ; i<=9; ++i)
            {
                if(CanAdd(grid, row, col, i))
                {
                    grid[row][col] = i;
                    if((col+1)<9)
                    {
                        if(SudoSolver(grid, row, col +1)) return 1;
                        else grid[row][col] = 0;
                    }
                    else if((row+1)<9)
                    {
                        if(SudoSolver(grid, row+1, 0)) return 1;
                        else grid[row][col] = 0;
                    }
                    else return 1;
                }
            }
        }
        return 0;
    }
    else return 1;
}

void Save(size_t grid[][9], char file_name[])
{
    FILE *out_file = fopen(file_name ,"w");
    if(out_file == NULL)
        printf("error out_file is NULL");
    for (size_t i = 1; i < 10 ; ++i) {
        for (size_t j = 1; j < 10 ; ++j) {
            fprintf(out_file,"%zu",grid[i -1][j -1]);
            if(j%3 == 0)
                fprintf(out_file," ");
        }
        fprintf(out_file,"\n");
        if (i%3 == 0)
            fprintf(out_file,"\n");

    }
    fclose(out_file);

}

void Load(char load_file[], size_t grid[][9])
{
    FILE *in_file = fopen(load_file, "r");
    if(in_file == NULL)
        printf("Error!");
    size_t i = 0;
    size_t j = 0;
    char c;
    int prev = 0;
    while ((c = fgetc(in_file)) != EOF)
    {
            if (c != ' ')
            {
                if(c == '\n' && !prev)
                {
                    ++i;
                    j = 0;
                    prev = 1;

                }
                else if (c == '.')
                {
                    grid[i][j] = 0;
                    ++j;
                    prev = 0;
                }
                else
                {

                    grid[i][j] = c - '0';
                    ++j;
                    prev = 0;
                }


            }

    }

    fclose(in_file);

}







#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include "SDL/SDL_image.h"
#include "ImageProc.h"
#include <SDL/SDL_rotozoom.h>



//int argc, char *argv[]
int main()
{
    SDL_Surface *ecran = NULL, *image = NULL, *rotation = NULL;
    SDL_Rect rect;
    SDL_Event event;

    double zoom = 1;
    double angle = 0;
    double angle2  ;

    printf( "Veuillez saisir un angle : " );
    int scanned =  scanf( "%lf", &angle2 );
    if (scanned == EOF) {

        SDL_FreeSurface(image);
        SDL_FreeSurface(rotation);
        SDL_Quit();

    }
     //va delete le \n char


    angle2 +=1;
    SDL_Init(SDL_INIT_VIDEO);


    ecran = SDL_SetVideoMode(500, 500, 32, SDL_HWSURFACE);
    SDL_WM_SetCaption("Faire des rotations avec SDL_gfx", NULL);

    image = SDL_LoadBMP("img/lena.bmp");

    while(angle <  angle2 )
    {
        SDL_PollEvent(&event);

        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));

        rotation = rotozoomSurface(image, angle, zoom, 0); //On transforme
        //la surface image.

        //On repositionne l'image en fonction de sa taille.
        rect.x =  event.button.x - rotation->w / 2;
        rect.y =  event.button.y - rotation->h / 2;

        SDL_BlitSurface(rotation , NULL, ecran, &rect); //On affiche la
        //rotation de la surface image.

        SDL_Flip(ecran);
        angle++;
    }

    if(SDL_SaveBMP(rotation, "result.bmp") != 0)
    {
        // Error saving bitmap
        printf("SDL_SaveBMP failed: %s\n", SDL_GetError());
    }
    else{
        printf("saved");
    }
    SDL_FreeSurface(image);
    SDL_FreeSurface(rotation);
    SDL_Quit();
}

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#ifndef OCR_SUDOKU_SOLVER_PRE_PROCESSING_H
#define OCR_SUDOKU_SOLVER_PRE_PROCESSING_H

SDL_Surface *filter(char *path, SDL_Surface** binary);

#endif //OCR_SUDOKU_SOLVER_PRE_PROCESSING_H

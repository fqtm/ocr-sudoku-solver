#ifndef OCR_SUDOKU_SOLVER_GRID_DETECTION_H
#define OCR_SUDOKU_SOLVER_GRID_DETECTION_H
struct couple{
    int x;
    int y;
};

struct line{
    int rho;
    int theta;
};

int iround(double value);
struct line* clean_lines(struct line* lines, size_t* nb_lines);
void RLSA(SDL_Surface* img, int threshold);
struct line* hough_transform(SDL_Surface* img, size_t* nblines);
struct line* get_sudoku_lines(SDL_Surface* img, struct line* lines, size_t* nb_lines);
void draw_line_green(SDL_Surface* img, struct line* line);
void draw_intersects(SDL_Surface* img, struct line* lines, size_t nblines);
struct couple* get_greater_polygon_corners(SDL_Surface* img,
                                           struct line* lines, size_t nblines, int* theta_rot);


#endif //OCR_SUDOKU_SOLVER_GRID_DETECTION_H
